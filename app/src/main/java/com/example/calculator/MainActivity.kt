package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private var num1 = 0.0
    private var num2 = 0.0
    private var operation = ""
    



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }


    private fun init() {
        if (resultTV.text.toString().isEmpty()){
            divideButton.isClickable = false
            multiplyButton.isClickable = false
            plusButton.isClickable = false
            minusButton.isClickable = false
        }

        button0.setOnClickListener(this)
        button1.setOnClickListener(this)
        button2.setOnClickListener(this)
        button3.setOnClickListener(this)
        button4.setOnClickListener(this)
        button5.setOnClickListener(this)
        button6.setOnClickListener(this)
        button7.setOnClickListener(this)
        button8.setOnClickListener(this)
        button9.setOnClickListener(this)

        delButton.setOnLongClickListener {
            val check = resultTV.text.toString()
            if (check.isNotEmpty()) {
                resultTV.text = ""
                divideButton.isClickable = false
                multiplyButton.isClickable = false
                plusButton.isClickable = false
                minusButton.isClickable = false
            }
            true
        }
    }

    fun delete(view: View) {
        val value = resultTV.text.toString()
        if (value.isNotEmpty()) {
            resultTV.text = value.substring(0, value.length - 1)
        }
        val check = resultTV.text.toString()
        if (check.isEmpty()){
            divideButton.isClickable = false
            multiplyButton.isClickable = false
            plusButton.isClickable = false
            minusButton.isClickable = false
        }
    }



    fun equal(view: View) {
        val value = resultTV.text.toString()
        if (value.isNotEmpty()) {
            num2 = value.toDouble()
            if (operation == "/") {
                if (num2 == 0.0) {
                    Toast.makeText(this, "You can't divide by zero", Toast.LENGTH_SHORT).show()
                } else {
                    resultTV.text = (num1 / num2).toString()
                    equalButton.isClickable = false
                }
            } else if (operation == "*") {
                resultTV.text = (num1 * num2).toString()
                equalButton.isClickable = false
            } else if (operation == "+") {
                resultTV.text = (num1 + num2).toString()
                equalButton.isClickable = false
            } else if (operation == "-") {
                resultTV.text = (num1 - num2).toString()
                equalButton.isClickable = false
            }
        }
    }

    fun divide(view: View) {
        operation = "/"
        val value = resultTV.text.toString()
        num1 = value.toDouble()
        resultTV.text = ""
    }

    fun minus(view: View) {

            operation = "-"
            val value = resultTV.text.toString()
            num1 = value.toDouble()
            resultTV.text = ""
    }

    fun plus(view: View) {

            operation = "+"
            val value = resultTV.text.toString()
            num1 = value.toDouble()
            resultTV.text = ""


    }

    fun multiply(view: View) {

            operation = "*"
            val value = resultTV.text.toString()
            num1 = value.toDouble()
            resultTV.text = ""

    }

    fun dot(view: View) {
        val value = resultTV.text.toString()
        if (value.isNotEmpty()) {
            if ("." in value) {
                Toast.makeText(this, "You can write only one dot", Toast.LENGTH_SHORT).show()
            } else {
                resultTV.text = "$value."
            }
        }
    }

    override fun onClick(v: View?) {
        val button = v as Button
        resultTV.text = resultTV.text.toString() + button.text.toString()
        divideButton.isClickable = true
        multiplyButton.isClickable = true
        plusButton.isClickable = true
        minusButton.isClickable = true
        equalButton.isClickable = true

    }
}